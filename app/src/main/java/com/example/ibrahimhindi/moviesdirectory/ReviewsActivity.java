package com.example.ibrahimhindi.moviesdirectory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReviewsActivity extends AppCompatActivity {
    RequestQueue request_queue;
    ListView reviews_list_view;
    ReviewsAdapter reviews_adapter;
    ArrayList<Review> reviews;
    ProgressBar reviews_loader;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

        request_queue = Volley.newRequestQueue(this);
        reviews_list_view = findViewById(R.id.reviews_list_view);
        reviews = new ArrayList<>();
        reviews_adapter = new ReviewsAdapter(this, R.layout.review, reviews);
        reviews_list_view.setAdapter(reviews_adapter);
        reviews_loader = findViewById(R.id.reviews_loader);

        int movie_id = getIntent().getIntExtra("movie_id", 0);
        url = "https://api.themoviedb.org/3/movie/" + movie_id + "/reviews?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US";
        fetchReviews();
    }

    public void print(String text){
        Log.v("AppLogs", text);
    }

    public void fetchReviews(){
        reviews_loader.setVisibility(ProgressBar.VISIBLE);
        reviews.clear();

        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        reviews_loader.setVisibility(ProgressBar.INVISIBLE);
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for (int i = 0; i < results.length(); i++){
                                JSONObject current_json = results.getJSONObject(i);
                                Review review = new Review();
                                review.author = current_json.getString("author");
                                review.content = current_json.getString("content");
                                reviews.add(review);
                            }

                            if(reviews.size() == 0){
                                Toast.makeText(ReviewsActivity.this, "No reviews found", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                reviews_adapter.notifyDataSetChanged();
                            }
                        }
                        catch (JSONException e) {
                            reviews_loader.setVisibility(ProgressBar.INVISIBLE);
                            print("JSON Error : " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        reviews_loader.setVisibility(ProgressBar.INVISIBLE);
                        print("Error : " + error.getMessage());
                    }
                }
        );

        request_queue.add(req);
    }
}
