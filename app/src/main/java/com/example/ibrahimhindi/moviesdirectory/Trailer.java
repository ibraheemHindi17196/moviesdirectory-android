package com.example.ibrahimhindi.moviesdirectory;

public class Trailer {
    String title;
    String url;
    String thumbnail_url;

    @Override
    public String toString() {
        return "Title: " + title + "\n" +
               "URL: " + url + "\n" +
               "Thumbnail URL: " + thumbnail_url + "\n";
    }
}
