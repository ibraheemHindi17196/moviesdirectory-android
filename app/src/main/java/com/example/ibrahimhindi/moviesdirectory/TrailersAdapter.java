package com.example.ibrahimhindi.moviesdirectory;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class TrailersAdapter extends ArrayAdapter<Trailer> {
    Context context;

    public TrailersAdapter(Context context, int resource_id, ArrayList<Trailer> items) {
        super(context, resource_id, items);
        this.context = context;
    }

    private class ViewHolder {
        ImageView image_view;
        TextView title_view;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TrailersAdapter.ViewHolder holder;
        Trailer item = getItem(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.trailer, null);
            holder = new TrailersAdapter.ViewHolder();
            holder.image_view = convertView.findViewById(R.id.trailer_image);
            holder.title_view = convertView.findViewById(R.id.trailer_title);
            convertView.setTag(holder);
        }
        else {
            holder = (TrailersAdapter.ViewHolder) convertView.getTag();
        }

        holder.title_view.setText(item.title);
        Picasso.get().load(item.thumbnail_url).into(holder.image_view);
        return convertView;
    }

}
