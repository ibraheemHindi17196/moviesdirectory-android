package com.example.ibrahimhindi.moviesdirectory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class MoviesActivity extends AppCompatActivity {
    RequestQueue request_queue;
    ListView movies_list_view;
    MoviesAdapter movies_adapter;
    ArrayList<Movie> movies;
    ProgressBar loader;
    MaterialSearchView search_view;
    SharedPreferences preferences;

    String displaying = "popular";
    String images_base_url = "https://image.tmdb.org/t/p/w1280";
    String most_popular = "https://api.themoviedb.org/3/movie/popular?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US&page=1";
    String top_rated = "https://api.themoviedb.org/3/movie/top_rated?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US&page=1";
    String now_playing = "https://api.themoviedb.org/3/movie/now_playing?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US&page=1";
    String upcoming = "https://api.themoviedb.org/3/movie/upcoming?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US&page=1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        // Initialize favorites count
        preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        if(!preferences.contains("favorites_count")){
            editor.putInt("favorites_count", 0);
            editor.apply();
        }

        // Clear shared preferences
        //editor.clear().commit();

        request_queue = Volley.newRequestQueue(this);
        movies = new ArrayList<>();
        movies_list_view = findViewById(R.id.movies_list_view);
        loader = findViewById(R.id.loader);
        movies_adapter = new MoviesAdapter(this, R.layout.movie, movies);
        movies_adapter.movies_activity = this;
        movies_list_view.setAdapter(movies_adapter);

        search_view = findViewById(R.id.search_view);
        search_view.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                displaying = "search";
                String url = "https://api.themoviedb.org/3/search/movie?query=" + query + "&page=1&api_key=22dffef7843a5d84428bc9f0ec48efb5";
                fetchMovies(url);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        fetchMovies(most_popular);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        search_view.setMenuItem(menu.findItem(R.id.action_search));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.most_popular:
                if(!displaying.equals("popular")){
                    displaying = "popular";
                    fetchMovies(most_popular);
                }
                return true;

            case R.id.top_rated:
                if(!displaying.equals("rated")){
                    displaying = "rated";
                    fetchMovies(top_rated);
                }
                return true;

            case R.id.now_playing:
                if(!displaying.equals("playing")){
                    displaying = "playing";
                    fetchMovies(now_playing);
                }
                return true;

            case R.id.upcoming:
                if(!displaying.equals("upcoming")){
                    displaying = "upcoming";
                    fetchMovies(upcoming);
                }
                return true;

            case R.id.favorites:
                if(!displaying.equals("favorites")){
                    displaying = "favorites";
                    displayFavorites();
                }
                return true;

            default:
                return false;
        }
    }

    public void print(String text){
        Log.v("AppLogs", text);
    }

    public void fetchMovies(String url) {
        loader.setVisibility(ProgressBar.VISIBLE);
        movies.clear();
        JsonObjectRequest req = new JsonObjectRequest(
            Request.Method.GET, url, null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loader.setVisibility(ProgressBar.INVISIBLE);
                    try {
                        JSONArray results = response.getJSONArray("results");
                        for (int i = 0; i < results.length(); i++){
                            JSONObject current_json = results.getJSONObject(i);
                            Movie movie = new Movie();
                            movie.id = current_json.getInt("id");
                            movie.title = current_json.getString("title");
                            movie.poster_path = images_base_url + current_json.getString("poster_path");
                            movie.overview = current_json.getString("overview");
                            movie.release_date = current_json.getString("release_date");
                            movie.vote_average = current_json.getDouble("vote_average");
                            movies.add(movie);
                        }

                        movies_adapter.notifyDataSetChanged();
                    }
                    catch (JSONException e) {
                        loader.setVisibility(ProgressBar.INVISIBLE);
                        print("JSON Error : " + e.getMessage());
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loader.setVisibility(ProgressBar.INVISIBLE);
                    print("Error : " + error.getMessage());
                }
            }
        );

        request_queue.add(req);
    }

    public void goToTrailers(int movie_id){
        Intent to_trailers = new Intent(MoviesActivity.this, TrailersActivity.class);
        to_trailers.putExtra("movie_id", movie_id);
        startActivity(new Intent(to_trailers));
    }

    public void goToReviews(int movie_id){
        Intent to_reviews = new Intent(MoviesActivity.this, ReviewsActivity.class);
        to_reviews.putExtra("movie_id", movie_id);
        startActivity(new Intent(to_reviews));
    }

    public void shareText(String text){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }

    // ------------------------- Favorites -------------------------

    public int getFavoriteIndex(int movie_id){
        Map<String, ?> all_entries = preferences.getAll();
        for (Map.Entry<String, ?> entry : all_entries.entrySet()) {
            if(entry.getKey().startsWith("Favorite_")){
                Gson gson = new Gson();
                String json = (String) entry.getValue();
                Movie current_movie = gson.fromJson(json, Movie.class);

                if (current_movie.id == movie_id){
                    return Integer.parseInt(entry.getKey().substring(9));
                }
            }
        }

        return -1;
    }

    public void displayFavorites(){
        movies.clear();

        Map<String, ?> all_entries = preferences.getAll();
        for (Map.Entry<String, ?> entry : all_entries.entrySet()) {
            if(entry.getKey().startsWith("Favorite_")){
                Gson gson = new Gson();
                String json = (String) entry.getValue();
                Movie current_movie = gson.fromJson(json, Movie.class);
                movies.add(current_movie);
            }
        }

        movies_adapter.notifyDataSetChanged();
        if(movies.size() == 0){
            Toast.makeText(MoviesActivity.this, "No favorites yet", Toast.LENGTH_SHORT).show();
        }
    }

    public void toggleLike(final Movie movie){
        int favorites_count = preferences.getInt("favorites_count", 0);
        SharedPreferences.Editor editor = preferences.edit();
        int favorite_index = getFavoriteIndex(movie.id);

        if(favorite_index == -1){
            // Add to favorites
            String object_name = "Favorite_" + favorites_count;
            Gson gson = new Gson();
            String json = gson.toJson(movie);
            editor.putString(object_name, json);
            editor.putInt("favorites_count", favorites_count+1);
            editor.apply();
        }
        else{
            // Remove from favorites
            editor.remove("Favorite_" + favorite_index);
            editor.putInt("favorites_count", favorites_count-1);
            editor.apply();

            if(displaying.equals("favorites")){
                movies.remove(movie);
                movies_adapter.notifyDataSetChanged();

                if(movies.size() == 0){
                    Toast.makeText(MoviesActivity.this, "No favorites yet", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
