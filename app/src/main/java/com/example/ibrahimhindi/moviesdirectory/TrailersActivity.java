package com.example.ibrahimhindi.moviesdirectory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TrailersActivity extends AppCompatActivity {
    RequestQueue request_queue;
    ListView trailers_list_view;
    TrailersAdapter trailers_adapter;
    ArrayList<Trailer> trailers;
    ProgressBar trailers_loader;
    String url;
    String youtube_base = "https://www.youtube.com/watch?v=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trailers);

        request_queue = Volley.newRequestQueue(this);
        trailers_list_view = findViewById(R.id.trailers_list_view);
        trailers = new ArrayList<>();
        trailers_adapter = new TrailersAdapter(this, R.layout.trailer, trailers);
        trailers_loader = findViewById(R.id.trailers_loader);
        trailers_list_view.setAdapter(trailers_adapter);

        trailers_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = trailers.get(position).url;
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });

        int movie_id = getIntent().getIntExtra("movie_id", 0);
        url = "https://api.themoviedb.org/3/movie/" + movie_id + "/videos?api_key=22dffef7843a5d84428bc9f0ec48efb5&language=en-US";
        fetchTrailers();
    }

    public void print(String text){
        Log.v("AppLogs", text);
    }

    public void fetchTrailers(){
        trailers_loader.setVisibility(ProgressBar.VISIBLE);
        trailers.clear();

        JsonObjectRequest req = new JsonObjectRequest(
                Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        trailers_loader.setVisibility(ProgressBar.INVISIBLE);
                        try {
                            JSONArray results = response.getJSONArray("results");
                            for (int i = 0; i < results.length(); i++){
                                JSONObject current_json = results.getJSONObject(i);

                                if(current_json.getString("type").equals("Trailer")){
                                    String key = current_json.getString("key");
                                    Trailer trailer = new Trailer();
                                    trailer.title = current_json.getString("name");
                                    trailer.url = youtube_base + key;
                                    trailer.thumbnail_url = "https://img.youtube.com/vi/" + key + "/hqdefault.jpg";
                                    trailers.add(trailer);
                                }
                            }

                            if(trailers.size() == 0){
                                Toast.makeText(TrailersActivity.this, "No trailers found", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                trailers_adapter.notifyDataSetChanged();
                            }
                        }
                        catch (JSONException e) {
                            trailers_loader.setVisibility(ProgressBar.INVISIBLE);
                            print("JSON Error : " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        trailers_loader.setVisibility(ProgressBar.INVISIBLE);
                        print("Error : " + error.getMessage());
                    }
                }
        );

        request_queue.add(req);
    }

}
