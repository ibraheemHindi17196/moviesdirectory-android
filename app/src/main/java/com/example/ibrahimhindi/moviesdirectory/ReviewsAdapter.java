package com.example.ibrahimhindi.moviesdirectory;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class ReviewsAdapter extends ArrayAdapter<Review> {
    Context context;

    public ReviewsAdapter(Context context, int resource_id, ArrayList<Review> items) {
        super(context, resource_id, items);
        this.context = context;
    }

    private class ViewHolder {
        TextView author_view;
        TextView content_view;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ReviewsAdapter.ViewHolder holder;
        Review item = getItem(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.review, null);
            holder = new ReviewsAdapter.ViewHolder();
            holder.author_view = convertView.findViewById(R.id.review_author);
            holder.content_view = convertView.findViewById(R.id.review_content);
            convertView.setTag(holder);
        }
        else {
            holder = (ReviewsAdapter.ViewHolder) convertView.getTag();
        }

        holder.author_view.setText(item.author);
        holder.content_view.setText(item.content);
        return convertView;
    }

}
