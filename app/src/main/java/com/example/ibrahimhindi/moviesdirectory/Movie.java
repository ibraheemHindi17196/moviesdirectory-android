package com.example.ibrahimhindi.moviesdirectory;

import android.graphics.Bitmap;

public class Movie {
    Integer id;
    String title;
    String poster_path;
    String overview;
    String release_date;
    Double vote_average;

    @Override
    public String toString() {
        return "ID: " + id + "\n" +
               "Title: " + title + "\n" +
               "Poster Path: " + poster_path + "\n" +
               "Overview: " + overview + "\n" +
               "Release Date: " + release_date + "\n" +
               "Vote Average: " + vote_average;
    }
}
