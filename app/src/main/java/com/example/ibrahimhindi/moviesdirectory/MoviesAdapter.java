package com.example.ibrahimhindi.moviesdirectory;

import android.app.Activity;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MoviesAdapter extends ArrayAdapter<Movie> {
    Context context;
    MoviesActivity movies_activity;

    public MoviesAdapter(Context context, int resource_id, ArrayList<Movie> items) {
        super(context, resource_id, items);
        this.context = context;
    }

    private class ViewHolder {
        ImageView image_view;
        TextView title_view;
        TextView rating_view;
        TextView release_date_view;
        TextView overview;
        ImageButton like;
        ImageButton share;
        ImageButton trailers;
        ImageButton reviews;
    }

    public void print(String text){
        Log.v("AppLogs", text);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final Movie item = getItem(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.movie, null);
            holder = new ViewHolder();
            holder.image_view = convertView.findViewById(R.id.image_view);
            holder.title_view = convertView.findViewById(R.id.title_view);
            holder.rating_view = convertView.findViewById(R.id.rating_view);
            holder.release_date_view = convertView.findViewById(R.id.release_date_view);
            holder.overview = convertView.findViewById(R.id.overview);
            holder.like = convertView.findViewById(R.id.like_button);
            holder.share = convertView.findViewById(R.id.share_button);
            holder.trailers = convertView.findViewById(R.id.trailers_button);
            holder.reviews = convertView.findViewById(R.id.reviews_button);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title_view.setText(item.title);
        holder.rating_view.setText(item.vote_average+"");
        holder.release_date_view.setText(item.release_date);
        holder.overview.setText(item.overview);
        Picasso.get().load(item.poster_path).into(holder.image_view);

        if(movies_activity.getFavoriteIndex(item.id) > -1){
            holder.like.setImageResource(R.drawable.like);
        }
        else{
            holder.like.setImageResource(R.drawable.unlike);
        }

        holder.trailers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movies_activity.goToTrailers(item.id);
            }
        });

        holder.reviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movies_activity.goToReviews(item.id);
            }
        });

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movies_activity.shareText("Here is a movie for you, " + item.title);
            }
        });

        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(movies_activity.getFavoriteIndex(item.id) > -1){
                    holder.like.setImageResource(R.drawable.unlike);
                }
                else{
                    holder.like.setImageResource(R.drawable.like);
                }

                movies_activity.toggleLike(item);
            }
        });

        return convertView;
    }

}